import logging
import os
import sys
import datetime

logger = None

def init(in_name_str, in_path_str=None, in_log_in_terminal=True , in_log_level=None, in_formatter_str = '[%(asctime)s: %(levelname)s] %(message)s'):
    global logger
    if in_log_level is None: in_log_level = logging.DEBUG
    logger = logging.getLogger(in_name_str)
    logger.setLevel(in_log_level)

    if in_path_str != None:
        if not os.path.exists(os.path.abspath(in_path_str)):
            os.mkdir(in_path_str)
        handler1 = logging.FileHandler(os.path.join(in_path_str, datetime.datetime.now().strftime("%Y_%m_%d") + ".log"),encoding ="UTF-8")
        handler1.setFormatter(logging.Formatter(fmt=in_formatter_str))
        logger.addHandler(handler1)
    if in_log_in_terminal == True:
        handler2 = logging.StreamHandler(stream=sys.stdout)
        handler2.setFormatter(logging.Formatter(fmt=in_formatter_str))
        logger.addHandler(handler2)
    return logger
	
	
def get():
    global logger
    return logger