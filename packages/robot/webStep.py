import time
import config
from pyOpenRPA.Robot import UIWeb
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def start_session(browser_path: str, driver_path: str, path_to_download:str, web_url:str):

    prefs = {
             "download.prompt_for_download": False,
             "profile.default_content_setting_values.automatic_downloads": 1,
             "download.default_directory": path_to_download,
             "savefile.default_directory": path_to_download,
             "download.directory_upgrade": True,
             "safebrowsing.enabled": True}

    options = Options()
    options.add_experimental_option('prefs', prefs)
    options.binary_location = browser_path

    driver = webdriver.Chrome(options=options, executable_path=driver_path)
    driver.maximize_window()
    UIWeb.BrowserChange(inBrowser=driver)
    UIWeb.PageOpen(web_url)


def end_session():
    UIWeb.BrowserClose()

    

def excel_download():
    # Нажать на кнопку DOWNLOAD EXCEL
    UIOSelector_str = '//a[@class=" col s12 m12 l12 btn waves-effect waves-light uiColorPrimary center"]'
    UIO = UIWeb.UIOSelectorList(inUIOSelectorStr = UIOSelector_str)[0]
    UIWeb.UIOClick(inUIO = UIO)



def fill_cards(info_lst:list):
    # Нажать на кнопку START
    UIOSelector_str = '//button[@class="waves-effect col s12 m12 l12 btn-large uiColorButton"]'
    UIO = UIWeb.UIOSelectorList(inUIOSelectorStr = UIOSelector_str)[0]
    UIWeb.UIOClick(inUIO = UIO)
    time.sleep(1)

    UIOSelector_str = '//input[@class="btn uiColorButton"]'
    UIO = UIWeb.UIOSelectorList(inUIOSelectorStr = UIOSelector_str)[0]
    for item in info_lst:
        UIWeb.PageJSExecute("$(`input[ng-reflect-name='labelAddress']`).val(arguments[0]);", item[4])
        UIWeb.PageJSExecute("$(`input[ng-reflect-name='labelPhone']`).val(arguments[0]);", item[6])
        UIWeb.PageJSExecute("$(`input[ng-reflect-name='labelRole']`).val(arguments[0]);", item[3])
        UIWeb.PageJSExecute("$(`input[ng-reflect-name='labelLastName']`).val(arguments[0]);", item[1])
        UIWeb.PageJSExecute("$(`input[ng-reflect-name='labelEmail']`).val(arguments[0]);", item[5])
        UIWeb.PageJSExecute("$(`input[ng-reflect-name='labelCompanyName']`).val(arguments[0]);", item[2])
        UIWeb.PageJSExecute("$(`input[ng-reflect-name='labelFirstName']`).val(arguments[0]);", item[0])

        UIWeb.UIOClick(inUIO = UIO)