import sys, datetime, pickle, os, calendar
import json
from importlib import util
import logging

from utils import logger

global_log = logger.init(in_name_str="robot log", in_path_str="logs", in_log_in_terminal=True , in_log_level=logging.DEBUG, in_formatter_str = '[%(asctime)s: %(levelname)s] %(message)s')

def __import_config__(inPyPathItemStr, inModuleNameStr):
    lPyPathItemStr = inPyPathItemStr
    lModuleNameStr = inModuleNameStr
    lTechSpecification = util.spec_from_file_location(lModuleNameStr, lPyPathItemStr)
    lTechModuleFromSpec = util.module_from_spec(lTechSpecification)
    sys.modules[lModuleNameStr] = lTechModuleFromSpec  # Add initialized module in sys - python will not init this module enought
    lTechSpecification.loader.exec_module(lTechModuleFromSpec)

__import_config__(sys.argv[1], "config") # Установить конфигурационный файл

import config # Настройка с конфига



from . import start

start.start(global_log)
        


