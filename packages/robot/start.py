
from . import webStep
from . import excelStep
import config
import time
import os
import traceback
import pyautogui



def start(global_log):
    counter = 0
    while counter < 2:
        try:
            global_log.info("########## СТАРТ ##########")
            webStep.start_session(config.BROWSERPATH, config.DRIVERPATH, config.DOWNLOADPATH, config.URL)
            global_log.info("Запуск сессии браузера - успешно")
            time.sleep(2)

            webStep.excel_download()
            start_point = time.time()
            while "challenge.xlsx" not in os.listdir(os.path.abspath("..")):
                if time.time() - start_point > 10: raise ValueError("НЕ УДАЛОСЬ СКАЧАТЬ ФАЙЛ")
                time.sleep(0.5)
            global_log.info("Скачивание excel файла - успешно")

            new_file_path = excelStep.rename_excel(os.path.abspath("..\\challenge.xlsx"))
            result=excelStep.get_info(new_file_path)
            global_log.info("Обработка excel файла - успешно")

            webStep.fill_cards(result)
            global_log.info("Заполнение карточек - успешно")

            time.sleep(2)
            screenshot = pyautogui.screenshot()
            screenshot.save(config.SCREENPATH)
            global_log.info("Снятие скриншота - успешно")


            webStep.end_session()
            global_log.info("Остановка сессии браузера - успешно")
            global_log.info("########## КОНЕЦ ##########")
            counter = 2
        except Exception:
            webStep.end_session()
            counter += 1
            if counter < 2:
                global_log.info(f"ПРОИЗОШЛА КРИТИЧЕСКАЯ ОШИБКА. РОБОТ ПЕРЕЗАПУСТИТСЯ И ПОВТОРИТ ОПЕРАЦИЮ ЧЕРЕЗ 10 СЕКУНД\n\n{traceback.format_exc()}")
                time.sleep(10)
            else:
                global_log.info(f"ПРОИЗОШЛА КРИТИЧЕСКАЯ ОШИБКА. РОБОТ ПРЕВЫСИЛ КОЛИЧЕСТВО ПОПЫТОК ВОССТАНОВЛЕНИЯ. РАБОТА ОСТАНОВЛЕНА\n\n{traceback.format_exc()}")
