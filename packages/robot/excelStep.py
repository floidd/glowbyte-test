import openpyxl
import uuid
import os


def rename_excel(file_path:str) -> str:
    excel_uuid = str(uuid.uuid4())[:5]
    try:
        os.rename(file_path, os.path.abspath(f"..\\challenge_{excel_uuid}.xlsx"))
        return os.path.abspath(f"..\\challenge_{excel_uuid}.xlsx")
    except Exception:
        return file_path


def get_info(file_path:str) -> list: 
    result_lst = []
    workbook = openpyxl.load_workbook(file_path)
    sheet = workbook.active
    for row in sheet.iter_rows(min_row=2, max_row=11, min_col=1, max_col=7, values_only=True):
        result_lst.append(row)
    return result_lst