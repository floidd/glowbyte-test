chcp 65001
@echo off
echo Формат использования init-python-env.cmd [имя запускаемого процесса.exe] [имя убиваемого процесса.exe]
echo Пример использования init-python-env.cmd orpa-rbt.exe orpa-rbt.exe

if [%1]==[] goto :python-env
goto create-exe
:create-exe
copy /Y "%~dp0..\resources\wpy64-31050\python-3.10.5.amd64\python.exe" "%~dp0..\resources\wpy64-31050\python-3.10.5.amd64\%1"
if [%2]==[] goto :python-env
goto taskkill
:taskkill
taskkill /im "%2" /F /fi "username eq %username%"
goto :python-env
:python-env
set CD_PREV=%cd%
cd /d "%~dp0..\resources\wpy64-31050\python-3.10.5.amd64"
set PATH=%cd%;%cd%\Scripts;%PATH%
cd /d "%~dp0..\packages"
set PYTHONPATH=%cd%;%PYTHONPATH%
cd %CD_PREV%
:eof
echo Инициализация Python окружения прошла успешно!
@echo on